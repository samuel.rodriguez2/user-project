const router = require("express").Router();
const { DBConnection } = require("../db/ConnectionObject");
const { queries } = require("../config/DBqueries.json");
const { QueryTypes } = require("sequelize");

router.post("/", async (req, res) => {
  DBConnection.query(queries.updatingUser, {
    replacements: {
      user_firstLastName: req.body.firstLastName,
      user_secondLastName: req.body.secondLastName,
      user_firstName: req.body.firstName,
      user_secondName: req.body.secondName,
      user_country: req.body.country,
      user_IDType: req.body.identificationType,
      user_IDNumber: req.body.identificationNumber,
      entrance_date: req.body.date,
      user_area: req.body.area,
      user_status: req.body.status,
      user_email: req.body.email,
      registration_date: req.body.registration_date,
      user_id: req.body.user_id,
    },
    type: QueryTypes.UPDATE,
  })
    .then(() => {
      return res.status(200).json({ message: "Successfull!" });
    })
    .catch((error) => {
      return res.status(500).send("here"+error);
      // return res.status(500).json({Erroressage: error})
    });
});

module.exports = {
  router,
};
