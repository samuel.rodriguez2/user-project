const router = require('express').Router();
const { QueryTypes } = require('sequelize');
const {queries} = require('../config/DBqueries.json');
const {DBConnection} = require('../db/ConnectionObject');

router.delete('/',async(req,res)=>{
    DBConnection.query(queries.deleteUserQuery, {
        replacements: {
            value: req.query.user_id
        },
        type: QueryTypes.DELETE
    }).then(()=>{
        return res.status(200).json({message: "successfull!"})
    }).catch((error)=>{
        return res.status(500).send(error)
    })

})
module.exports = {
    router
}