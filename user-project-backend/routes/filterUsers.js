const router = require("express").Router();
const { DBConnection } = require("../db/ConnectionObject");
const { queries } = require("../config/DBqueries.json");
const { QueryTypes } = require("sequelize");

router.get("/", async (req, res) => {
  switch (Object.keys(req.query)[0]) {
    case "user_firstName":
      DBConnection.query(queries.filteringUsersByFirstName, {
        replacements: {
          value: req.query.user_firstName,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        });      
      break;
    case "user_secondName":
      DBConnection.query(queries.filteringUsersBySecondName, {
        replacements: {
          value: req.query.user_secondName,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        }); 
      break;
    case "user_firstLastName":
      DBConnection.query(queries.filteringUsersByFirstLastName, {
        replacements: {
          value: req.query.user_firstLastName,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        }); 
      break;
    case "secondLastName":
      DBConnection.query(queries.filteringUsersBySecondLastName, {
        replacements: {
          value: req.query.user_secondLastName,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        }); 
    case "user_IDType":
      DBConnection.query(queries.filteringUsersByIDType, {
        replacements: {
          value: req.query.user_IDType,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        }); 
      break;
    case "user_IDNumber":
      DBConnection.query(queries.filteringUsersByIDNumber, {
        replacements: {
          value: req.query.user_IDNumber,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        }); 
      break;
    case "user_country":
      DBConnection.query(queries.filteringUsersByCountry, {
        replacements: {
          value: req.query.user_country,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        }); 
      break;
    case "user_email":
      DBConnection.query(queries.filteringUsersByEmail, {
        replacements: {
          value: req.query.user_email,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        }); 
      break;
    case "user_status":
      DBConnection.query(queries.filteringUsersByStatus, {
        replacements: {
          value: req.query.user_status,
        },
        type: QueryTypes.SELECT,
      })
        .then((result) => {
          return res.status(200).send(result);
        })
        .catch((error) => {
          return res.status(500).send("error" + error);
        }); 
      break;
    default:
      break;
  }
  // console.log(finalQuery);
  // DBConnection.query(queries.filteringBySeveralQueries, {
  //   replacements: {
  //     value: queryToSearch,
  //   },
  //   type: QueryTypes.SELECT,
  // })
  //   .then((result) => {
  //     return res.status(200).send(result);
  //   })
  //   .catch((error) => {
  //     return res.status(500).send("error" + error);
  //   });
});
module.exports = {
  router,
};
