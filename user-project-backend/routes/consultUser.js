const router = require("express").Router();
const { DBConnection } = require("../db/ConnectionObject");
const { queries } = require("../config/DBqueries.json");
const { QueryTypes } = require("sequelize");
router.get("/", async (req, res) => {
  DBConnection.query(queries.consultUsersQuery, { type: QueryTypes.SELECT })
    .then((result) => {
      return res.status(200).send(result);
    })
    .catch((error) => {
      return res
        .status(500)
        .send("Error when returning the DB information" + error);
    });
});

router.get("/checkingID", async (req, res) => {
  DBConnection.query(queries.checkingIDNumber, {
    replacements: {
      user_IDNumber: req.query.identificationNumber,
    },
    type: QueryTypes.SELECT,
  })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((error) => {
      return res
        .status(500)
        .send("Error while trying to get the informatin" + error);
    });
});

router.get("/checkingEmail",async(req,res)=>{
  DBConnection.query(queries.checkingEmail, {
    replacements: {
      regexexp: `^${req.query.email}`,
    },
    type: QueryTypes.SELECT,
  })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((error) => {
      return res
        .status(500)
        .send("Error while trying to get the informatin" + error);
    });
})

module.exports = {
  router,
};
