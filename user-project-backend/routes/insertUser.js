const router = require("express").Router();
const { DBConnection } = require("../db/ConnectionObject");
const { queries } = require("../config/DBqueries.json");
const { QueryTypes } = require("sequelize");
router.post("/", async (req, res) => {
  console.log(req.body);
  DBConnection.query(queries.insertingUser, {
    replacements: {
      user_firstLastName: req.body.firstLastName,
      user_secondLastName: req.body.secondLastName,
      user_firstName: req.body.firstName,
      user_secondName: req.body.secondName,
      user_country: req.body.country,
      user_IDType: req.body.identificationType,
      user_IDNumber: req.body.identificationNumber,
      entrance_date: req.body.date,
      user_area: req.body.area,
      user_status: req.body.status,
      user_email: req.body.email,
      registration_date: req.body.registration_date,
    },
    type: QueryTypes.INSERT,
  })
    .then(() => {
      return res.status(200).json({ message: "Sucessfull!" });
    })
    .catch((error) => {
      return res.status(500).send("Error" + error);
    });
});

module.exports = {
  router,
};
