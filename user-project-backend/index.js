const config = require("config");
const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const cors = require("cors");

const server = express();

server.use(bodyParser.json());
server.use(cors());

const listingUsersRouter = require("./routes/consultUser").router;
server.use("/api/v1/user_project/users/consult", listingUsersRouter);

const insertingUserRouter = require("./routes/insertUser").router;
server.use("/api/v1/user_project/users/insert", insertingUserRouter);

const updatingUserRouter = require("./routes/updateUser").router;
server.use("/api/v1/user_project/users/update", updatingUserRouter);

const filteringUserRouter = require("./routes/filterUsers").router;
server.use("/api/v1/user_project/users/filter", filteringUserRouter);

const deleteUserRouter = require("./routes/deleteUser").router;
server.use("/api/v1/user_project/users/delete", deleteUserRouter);

http.createServer(server).listen(config.port, () => {
  console.log(`Server listening on port ${config.port}`);
});
