const { DBConnection } = require("./ConnectionObject");
const { queries } = require("../config/DBqueries.json");
const { data } = require("../config/users.json");
const { QueryTypes } = require("sequelize");
let finalQuery = queries.initialazingUserDB;
data.forEach((element) => {
  finalQuery += `('${element.user_name}','${element.user_email}','${element.user_phone}'),`;
});

DBConnection.query(finalQuery.substring(0, finalQuery.length - 1), {
  type: QueryTypes.INSERT,
})
  .then(() => console.log("Successfull"))
  .catch((error) => {
    console.log(error);
    console.log("ERror");
  });
