CREATE DATABASE user_project;
-- It is needed to create a new user to be able to use the DB
CREATE USER 'user_name'@'localhost' IDENTIFIED BY 'user_password';
-- Granting privilages
GRANT ALL PRIVILEGES ON * . * TO 'user_name'@'localhost';
FLUSH PRIVILEGES;

USE user_project;


CREATE TABLE IF NOT EXISTS users (
    user_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_firstLastName VARCHAR(32) NOT NULL,
    user_secondLastName VARCHAR(32) NOT NULL,
    user_firstName VARCHAR(32) NOT NULL,
    user_secondName VARCHAR(32),
    user_country VARCHAR(32) NOT NULL,
    user_IDType VARCHAR(32) NOT NULL,
    user_IDNumber VARCHAR(32) NOT NULL UNIQUE,
    entrance_date VARCHAR(32) NOT NULL,
    user_area VARCHAR(32) NOT NULL,
    user_status VARCHAR(32) NOT NULL,
    user_email VARCHAR(32) NOT NULL UNIQUE,
    registration_date VARCHAR(32) NOT NULL
);