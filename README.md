# User Project


Gracias por la oportunidad de participar en este proceso.


# User Project FrontEnd

Esta aplicacion fue construida usando Angular, es entrar en su directorio user-project-front-end/ y ejecutar npm ci. Este comando eliminaráa los node modules existentes y los instalará nuevamente.Una vez termina este proceso es necesario correr ng serve.

La aplicacion estarpa corriendo en el localhost:3000, abriendo el navegador y escribiendo dicha direccióón permirá ver la aplicación.
Si se encuentran errores de can not get source file. Sera necesario ir a src/app/app.module.ts y cortar el contenido de providers: [], guardar, copiar de nuevo su contenido dentro de providers:[], guardar y refrescar el navegador.

La aplicacion ha sido planeada para tener 2 vistas. Una de ellas permite Registrar los nuevos empleados, creando de manera automatica su correo electrónico y guardar la fecha y hora del momento en el que se hizo el registro en la base de datos. 

De igual forma es capaz de validar si el ID existe y asegurarse de que el email no se repita. 

En la vista de consult, se puede ver todos los registros disponibles, editarlos, eliminarlos y filtrarlos. 

En ambas vistas se usarn group forms para captar la informacion, validadores propios de angular para validar que la informacion cumpla con los requerimientos y para enviar los requests al backend diseñado especificamente para responder a estos requerimientos. 

Todos los requests hechos al backend estan escritos en el backend.service.ts

Se escribieron varisa funciones para manejar los eventos de envio, vuelta atrás, validacion entre otros.

# User Project Backend

Es necesario empezar con la Base de Datos, una vez se tenga el servidor sql, es necesario conectarse a el usando mysql -u nombre_de_usuario -p. Una vez en la consola de mysql, es necesario ejecutar los comandos de createDB.sql el cual estan dentro de user-project-backend/db/. Las lineas 3 a la 6 son para crear un usuario si no se tiene, y habiendose conectado al servidor sql como administrador previamente. Teniendo en cuenta que en este momento nos estamos conectando con un usuario previamente creado, se pueden omitir dichas lineas.


Para correr este projecto es necesario dirigirse al directorio user-project-backend, y ejecutar npm ci, export NODE_ENV=development y luego node index.js


Esta parte de la aplicacion fue construida usando node js y la libreria express. Se usaron routers para organizar los endpoints.Todos ellos organizados usando /api/v1/user_project/users/ para organizar insert,consult,delete,edit. 

Agradezco la atencion, el tiempo y la considreacion.

Quedo atento al feedback por parte de ustedes,

Gracias.
