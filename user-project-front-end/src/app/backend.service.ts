import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class BackendService {
  url = 'http://localhost:5500/api/v1/user_project/users/consult'
  postURL = 'http://localhost:5500/api/v1/user_project/users/insert'
  updateURL = 'http://localhost:5500/api/v1/user_project/users/update'
  filterURL = 'http://localhost:5500/api/v1/user_project/users/filter'
  deleteURL = 'http://localhost:5500/api/v1/user_project/users/delete'

  constructor(private http: HttpClient) { }
  public getUsers() {
    return this.http.get(this.url)
  }
  public checkingID(query: string) {
    return this.http.get(this.url + `/checkingID?identificationNumber=${query}`)
  }
  public checkingEmail(query: string) {
    return this.http.get(this.url + `/checkingEmail?email=${query}`)
  }
  public addingTheUser (user: any) {
    return this.http.post<any>(this.postURL,user)
  }
  public updateTheUser (user: any) {
    return this.http.post<any>(this.updateURL,user)
  }
  public filteringUsers (key: String, value: String) {
    return this.http.get(`${this.filterURL}?${key}=${value}`)
  }
  public deleteUser (user_id: any) {
    return this.http.delete(`${this.deleteURL}?user_id=${user_id}`)
  }
}
