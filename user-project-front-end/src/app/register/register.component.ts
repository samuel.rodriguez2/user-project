
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, Form } from '@angular/forms'
import { BackendService } from '../backend.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  nameRegex: RegExp;
  secondNameRegex: RegExp;
  identificationNumberRegex: RegExp;
  auxIDChecker: any;
  flag: boolean;
  constructor(private fb: FormBuilder, private http: BackendService) {
    this.flag = false;
    this.nameRegex = /^[\sA-Z]{1,20}$/
    this.secondNameRegex = /^[\sA-Z]{1,50}$/
    this.identificationNumberRegex = /^([aA-zZ0-9\-]*)$/
    this.registerForm = this.fb.group({
      firstLastName: [null, [Validators.required, Validators.pattern(this.nameRegex)]],
      secondLastName: [null, [Validators.required, Validators.pattern(this.nameRegex)]],
      firstName: [null, [Validators.required, Validators.pattern(this.nameRegex)]],
      secondName: [null, [Validators.pattern(this.secondNameRegex)]],
      country: [null, Validators.required],
      identificationType: [null, Validators.required],
      identificationNumber: [null, [Validators.required, Validators.pattern(this.identificationNumberRegex)]],
      date: [null, [Validators.required, this.dateValidator]],
      area: [null, Validators.required],
      status: ['Active', Validators.required],
      email: [null],
      registration_date: ['']
    })
  }
  get firstLastName() {
    return this.registerForm.get('firstLastName');
  }
  get secondLastName() {
    return this.registerForm.get('secondLastName');
  }
  get firstName() {
    return this.registerForm.get('firstName')
  }
  get secondName() {
    return this.registerForm.get('secondName')
  }
  get country() {
    return this.registerForm.get('country')
  }

  get identificationType() {
    return this.registerForm.get('identificationType')
  }
  get identificationNumber() {
    return this.registerForm.get('identificationNumber')
  }
  get date() {
    return this.registerForm.get('date');
  }
  get area() {
    return this.registerForm.get('area')
  }
  get status() {
    return this.registerForm.get('status')
  }
  dateValidator(fc: FormControl) {
    var date = new Date();
    date.setDate(date.getDate() - 30);
    const dateString = date.toISOString().split('T')[0];
    const today = new Date().toISOString().split('T')[0];
    const value = fc.value as string;
    const isInvalid = value < dateString || value > today;
    return isInvalid ? { dateError: "Date is out of range" } : null;
  }
  checkingID() {
    this.http.checkingID(this.registerForm.value.identificationNumber).subscribe((data) => {
      this.auxIDChecker = data;
      if (this.auxIDChecker.length > 0) {
        this.flag = true;
      } else {
        this.flag = false;
      }

    })
  }
  creatingEmail(fn: String, fln: String, c: String) {
    let finalValue = '';
    finalValue += fn + '.' + fln;
    this.http.checkingEmail(finalValue).subscribe((data) => {
      const auxiliarHere = data
      if (auxiliarHere.length === 0) {
        if (c === 'Colombia') {
          finalValue += '@cidenet.com.co'
        } else if (c === 'United States') {
          finalValue += '@cidenet.com.us'
        }
        this.registerForm.value.email = finalValue.toLowerCase();
      } else {
        finalValue += auxiliarHere.length
        if (c === 'Colombia') {
          finalValue += '@cidenet.com.co'
        } else if (c === 'United States') {
          finalValue += '@cidenet.com.us'
        }
        this.registerForm.value.email = finalValue.toLowerCase();

      }
      this.gettingCurrentDate();
      this.http.addingTheUser(this.registerForm.value).subscribe({
        next: () => {
          window.alert("Successfully User Registered")
        },
        error: (error) => {
          console.log("there was an error", error)
        }
      })

    })
  }
  gettingCurrentDate() {
    const todaysDateObject = new Date()
    this.registerForm.value.registration_date = `${todaysDateObject.getFullYear()}-${todaysDateObject.getMonth() + 1
      }-${todaysDateObject.getDate()} ${todaysDateObject.getHours()
      }:${todaysDateObject.getMinutes()}:${todaysDateObject.getSeconds()}`
  }
  onSubmit() {
    if (this.flag) {
      window.alert("The ID Number entered is already resgistered")
    } else {
      this.creatingEmail(this.registerForm.value.firstName, this.registerForm.value.firstLastName, this.registerForm.value.country);
    }
  }
  ngOnInit(): void { }
}