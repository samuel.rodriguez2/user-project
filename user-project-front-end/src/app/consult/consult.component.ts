import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-consult',
  templateUrl: './consult.component.html',
  styleUrls: ['./consult.component.css'],
})
export class ConsultComponent implements OnInit {
  users: any;
  userEdited: any;
  flag: boolean;
  IDFlag: boolean;
  nameRegex: RegExp;
  secondNameRegex: RegExp;
  identificationNumberRegex: RegExp;
  filterForm: FormGroup;
  auxIDChecker: any;
  editForm: FormGroup;
  constructor(private http: BackendService, private fb: FormBuilder) {
    this.IDFlag = false;
    this.flag = false;
    this.nameRegex = /^[\sA-Z]{1,20}$/;
    this.secondNameRegex = /^[\sA-Z]{1,50}$/;
    this.identificationNumberRegex = /^([aA-zZ0-9\-]*)$/;
    this.filterForm = this.fb.group({
      key: [null, Validators.required],
      value: [null, Validators.required]
    })
    this.editForm = this.fb.group({
      firstLastName: [
        null,
        [Validators.required, Validators.pattern(this.nameRegex)]
      ],
      secondLastName: [
        null,
        [Validators.required, Validators.pattern(this.nameRegex)]
      ],
      firstName: [
        null,
        [Validators.required, Validators.pattern(this.nameRegex)]
      ],
      secondName: [null, [Validators.pattern(this.secondNameRegex)]],
      country: [null, Validators.required],
      identificationType: [null, Validators.required],
      identificationNumber: [
        null,
        [
          Validators.required,
          Validators.pattern(this.identificationNumberRegex),
        ]
      ],
      date: [null, [Validators.required, this.dateValidator]],
      area: [null, Validators.required],
      status: ['Active', Validators.required],
      email: [null],
      registration_date: [''],
      user_id: [null],

    });
  }
  get key() {
    return this.filterForm.get('key')
  }
  get value() {
    return this.filterForm.get('value')
  }
  get firstLastName() {
    return this.editForm.get('firstLastName');
  }
  get secondLastName() {
    return this.editForm.get('secondLastName');
  }
  get firstName() {
    return this.editForm.get('firstName');
  }
  get secondName() {
    return this.editForm.get('secondName');
  }
  get country() {
    return this.editForm.get('country');
  }

  get identificationType() {
    return this.editForm.get('identificationType');
  }
  get identificationNumber() {
    return this.editForm.get('identificationNumber');
  }
  get date() {
    return this.editForm.get('date');
  }
  get area() {
    return this.editForm.get('area');
  }
  get status() {
    return this.editForm.get('status');
  }
  dateValidator(fc: FormControl) {
    var date = new Date();
    date.setDate(date.getDate() - 30);
    const dateString = date.toISOString().split('T')[0];
    const today = new Date().toISOString().split('T')[0];
    const value = fc.value as string;
    const isInvalid = value < dateString || value > today;
    return isInvalid ? { dateError: 'Date is out of range' } : null;
  }
  checkingID() {
    if (this.identificationNumber.dirty) {
      this.http
        .checkingID(this.editForm.value.identificationNumber)
        .subscribe((data) => {
          this.auxIDChecker = data;
          if (this.auxIDChecker.length > 0) {
            this.IDFlag = true;
          } else {
            this.IDFlag = false;
          }
        });
    }
  }
  togglingFlag(e: Event) {
    e.preventDefault();
    this.flag = !this.flag;
  }

  populatingEditingView(e: Event) {
    if (e.target !== null) {
      this.users.forEach((element: any) => {
        if (parseInt(element.user_id) === parseInt(e.target.id)) {
          this.userEdited = element;
        }
      });
    }
    this.editForm.setValue({
      firstLastName: this.userEdited.user_firstLastName.toUpperCase(),
      secondLastName: this.userEdited.user_secondLastName.toUpperCase(),
      firstName: this.userEdited.user_firstName.toUpperCase(),
      secondName: this.userEdited.user_secondName,
      country: this.userEdited.user_country,
      identificationType: this.userEdited.user_IDType,
      identificationNumber: this.userEdited.user_IDNumber,
      date: this.userEdited.entrance_date,
      area: this.userEdited.user_area,
      status: this.userEdited.user_status,
      email: this.userEdited.user_email,
      registration_date: this.userEdited.registration_date,
      user_id: e.target.id,
    });
  }
  goingForward(e: Event) {
    this.populatingEditingView(e);
    this.togglingFlag(e);
  }
  goingBack(e: Event) {
    e.preventDefault();
    this.http.getUsers().subscribe({
      next: (data) => {
        this.users = data;
        this.togglingFlag(e);
      },
      error: (error) => {
        console.log(error);
        window.alert('There was an error going back');
      },
    });
  }
  creatingEmail(fn: String, fln: String, c: String) {
    let finalValue = '';
    finalValue += fn + '.' + fln;
    this.http.checkingEmail(finalValue).subscribe((data) => {
      const auxiliarHere = data;
      if (auxiliarHere.length === 0) {
        if (c === 'Colombia') {
          finalValue += '@cidenet.com.co';
        } else if (c === 'United States') {
          finalValue += '@cidenet.com.us';
        }
        this.editForm.value.email = finalValue.toLowerCase();
      } else {
        finalValue += auxiliarHere.length;
        if (c === 'Colombia') {
          finalValue += '@cidenet.com.co';
        } else if (c === 'United States') {
          finalValue += '@cidenet.com.us';
        }
        this.editForm.value.email = finalValue.toLowerCase();
      }
      this.http.updateTheUser(this.editForm.value).subscribe({
        next: () => {
          window.alert('Successfully User Updated');
        },
        error: (error) => {
          console.log('there was an error', error);
        },
      });
    });
  }
  gettingCurrentDate() {
    const todaysDateObject = new Date();
    this.editForm.value.registration_date = `${todaysDateObject.getFullYear()}-${todaysDateObject.getMonth() + 1
      }-${todaysDateObject.getDate()} ${todaysDateObject.getHours()}:${todaysDateObject.getMinutes()}:${todaysDateObject.getSeconds()}`;
  }

  onSubmit() {
    if (
      this.IDFlag &&
      this.editForm.value.identificationNumber !== this.userEdited.user_IDNumber
    ) {
      window.alert('The ID number is already registered');
    } else {
      this.gettingCurrentDate();
      if (
        this.firstName.dirty ||
        this.firstLastName.dirty ||
        this.country.dirty
      ) {
        this.creatingEmail(
          this.editForm.value.firstName,
          this.editForm.value.firstLastName,
          this.editForm.value.country
        );
      } else {
        this.http.updateTheUser(this.editForm.value).subscribe({
          next: () => {
            window.alert('Successfully User Updated');
          },
          error: (error) => {
            console.log('there was an error', error);
          },
        });
      }
    }
  }
  sendingQuery(e: Event) {
    e.preventDefault();
    this.http.filteringUsers(this.filterForm.value.key, this.filterForm.value.value).subscribe((data) => {
      this.users = data
    })
  }
  resetFilter(e: Event) {
    e.preventDefault();
    this.http.getUsers().subscribe((data) => {
      this.users = data;
    })
  }
  deleteUser(e: Event) {
    e.preventDefault();
    const thisConfirmation = window.confirm("Are you sure you want to delete user?")
    if (thisConfirmation) {
      this.http.deleteUser(e.target.id).subscribe({
        next: () => {
          window.alert("User Succesfully deleted")
          this.resetFilter(e);
        },
        error: (error) => {
          console.log(error)
        }
      })

    } else {
      console.log("Nothing done")

    }


  }
  ngOnInit(): void {
    this.http.getUsers().subscribe((data) => {
      this.users = data;
    });
  }
}
